package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private HashMap<String,Double> quoterValues = new HashMap<>();

    public Quoter() {
        quoterValues.putIfAbsent("1", 10.0);
        quoterValues.putIfAbsent("2", 45.0);
        quoterValues.putIfAbsent("3", 20.0);
        quoterValues.putIfAbsent("4", 35.0);
        quoterValues.putIfAbsent("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        return quoterValues.getOrDefault(isbn, 0.0);
    }
}
